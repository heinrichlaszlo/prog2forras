#include <iostream>

using namespace std;

class Madar {
public:
	virtual void repul(){
		cout<<"Tudok repülni!"<<endl;
	};
};

class Program {
public:
	void fgv (Madar &madar){
		madar.repul();
	}
};

class Pingvin : public Madar
{};

int main (int argc, char **argv)
{
	Program program;
	Madar madar;
	program.fgv (madar);

	Pingvin pingvin;
	program.fgv (pingvin);
}
